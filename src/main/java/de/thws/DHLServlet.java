package de.thws;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/DHLServlet")
public class DHLServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String lengthString = request.getParameter("length");
		String widthString = request.getParameter("width");
		String heightString = request.getParameter("height");
		String weightString = request.getParameter("weight");
		int length = Integer.parseInt(lengthString);
		int width = Integer.parseInt(widthString);
		int height = Integer.parseInt(heightString);
		int weight = Integer.parseInt(weightString);

		String result = "<html><head><title>Shipping Cost Calculator</title></head>\n"
				+ "<body>\n"
				+ "  <h1>Shipping Cost Calculator</h1>"
				+ "<div id=\"price\">";
		try
		{
			int price = new DHL().getPriceForDimensions(length, width, height, weight);
			result = result + price;
		}
		catch(NotDeliverableException e)
		{
			result = result + "Package not deliverable";
		}
		result += "</body></html>";
		response.getWriter().append(result);
		
	}

}
