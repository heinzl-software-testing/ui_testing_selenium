package de.thws;

public class DHL
{

  public int getPriceForDimensions(int length, int width, int height, int weight)
  {
    checkDimension(length, "Length");
    checkDimension(width, "Width");
    checkDimension(height, "Height");
    checkDimension(weight, "Weight");

    if (height <= 15 && width <= 30 && length <= 60
        && weight <= 2000) return 549;
    else if (height <= 60 && width <= 60 && length <= 120)
    {
      if (weight <= 5000) return 699;
      else if (weight <= 10000) return 949;
      else if (weight <= 31500) return 1649;
    }
    throw new NotDeliverableException();
  }

  private static void checkDimension(int dimension, String dimensionName)
  {
    if (dimension <= 0) throw new NotDeliverableException(dimensionName + " must be bigger than 0");
  }
}
