package de.thws;

public class NotDeliverableException extends RuntimeException
{
  public NotDeliverableException() {
  }

  public NotDeliverableException(String message) {
    super(message);
  }

  public NotDeliverableException(String message, Throwable cause) {
    super(message, cause);
  }

  public NotDeliverableException(Throwable cause) {
    super(cause);
  }

  public NotDeliverableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
