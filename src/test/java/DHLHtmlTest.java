import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DHLHtmlTest
{
  ChromeDriver driver;

  @BeforeAll
  static void init()
  {
    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/resources/chromedriver.exe");
  }

  @BeforeEach
  void setup()
  {
    //    ChromeOptions options = new ChromeOptions();
    //    options.addArguments("--headless=new");
    //    driver = new ChromeDriver(options);
    driver = new ChromeDriver();
  }

  @Test
  void test()
  {
    String webappURL = "http://localhost:8080/_70_UI_Testing_With_Selenium_war_exploded/";
    driver.get(webappURL + "DHL.html");

    String title = driver.getTitle();
    assertEquals("Shipping Cost Calculator", title);

    driver.manage().timeouts().implicitlyWait(Duration.ofMillis(2000)); //polls for the elements for up to 2 seconds

    WebElement lengthInput = driver.findElement(By.id("length"));
    WebElement widthInput = driver.findElement(By.id("width"));
    WebElement heightInput = driver.findElement(By.id("height"));
    WebElement weightInput = driver.findElement(By.id("weight"));
    WebElement submitButton = driver.findElement(By.id("calculate"));

    lengthInput.sendKeys("30");
    widthInput.sendKeys("15");
    heightInput.sendKeys("8");
    weightInput.sendKeys("1000");
    submitButton.click();

    assertEquals(webappURL + "DHLServlet", driver.getCurrentUrl());
    WebElement priceDiv = driver.findElement(By.id("price"));
    String value = priceDiv.getText();
    assertEquals("549", value);
  }

  @AfterEach
  void tearDown()
  {
    driver.quit();
  }
}
